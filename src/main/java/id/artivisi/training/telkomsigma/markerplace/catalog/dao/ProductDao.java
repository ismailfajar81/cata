package id.artivisi.training.telkomsigma.markerplace.catalog.dao;

import id.artivisi.training.telkomsigma.markerplace.catalog.entity.Product;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProductDao extends PagingAndSortingRepository<Product, String> {
}
